
#include <ros/ros.h>
#include "src/nodegen.h"

using namespace saslam;

int main(int argc, char **argv)
{


    ros::init(argc, argv, "sa_nodegen");

    nodegen nodegen_class = nodegen();
    ros::spin();

    return 0;
}
