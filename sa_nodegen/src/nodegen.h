/** @file nodegen.h
    @date 2018/12
    @author Seungwon Song
    @brief  Generate node which contain sensor data
*/

#ifndef NODEGEN_H
#define NODEGEN_H

#include <ros/ros.h>
#include <sstream>

#include <std_msgs/Header.h>
#include <sensor_msgs/PointCloud.h>
#include <nav_msgs/Odometry.h>
#include "saslam/sa_node.h"
#include "saslam/sa_laser.h"
#include "unavlib/convt.h"

namespace saslam
{
class nodegen
{
private:

  ros::NodeHandle m_nh;/**< Ros node handler */
  ros::Publisher m_pub_node;/**< Node publisher */
  ros::Publisher m_pub_debug_ptcl;

  float m_param_nodeDist; // Meter
  float m_param_nodeAngle; // Degree
  float m_param_maxLaser; // Meter
  float m_param_minLaser; // Degree
  float m_param_timeSync;

  bool m_flag_odom;
  bool m_flag_laser;
  int m_node_idx;

  /*sensor data container*/
  std::vector<saslam::sa_laser> m_laser;
  std::vector<nav_msgs::Odometry> m_odom;

  Eigen::Matrix4f m_odom_before;

  void getparam();

  void callback_laser(const saslam::sa_laser::ConstPtr& msg);
  void callback_odom(const nav_msgs::Odometry::ConstPtr& msg);
  bool update(sa_node& nodeOut);


public:
  nodegen();
  ~nodegen();



};

}


#endif

