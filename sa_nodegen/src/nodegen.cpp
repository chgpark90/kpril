#include "nodegen.h"

using namespace saslam;
using namespace unavlib;
std::vector<std::pair<int,double>> timediff(std::vector<std::pair<int,std_msgs::Header>> times);

nodegen::nodegen()
{
    static ros::Subscriber sub1 = m_nh.subscribe<sa_laser>("/laser",1000,&nodegen::callback_laser,this);
    static ros::Subscriber sub3 = m_nh.subscribe<nav_msgs::Odometry>("/odom",1000,&nodegen::callback_odom,this);
    m_pub_node = m_nh.advertise<sa_node>("saslam/nodegen/combinenode",100);
    m_pub_debug_ptcl = m_nh.advertise<sensor_msgs::PointCloud2>("saslam/nodegen/debug/pointcloud",100);
    m_odom_before = Eigen::Matrix4f::Identity();
    getparam();
    m_node_idx = 0;
}

nodegen::~nodegen()
{

}

void nodegen::callback_laser(const saslam::sa_laser::ConstPtr& msg)
{
    m_laser.push_back(*msg);
    sa_node nodeOut;
    if(update(nodeOut)) {
        m_pub_node.publish(nodeOut);
    }
}


void nodegen::callback_odom(const nav_msgs::Odometry::ConstPtr &msg)
{
    m_odom.push_back(*msg);
    sa_node nodeOut;
    if(update(nodeOut)) {
        m_pub_node.publish(nodeOut);
    }
}

bool nodegen::update(sa_node &nodeOut) //If all data is prepared, generate combined node.
{
    while((!m_flag_laser || !m_laser.empty())&&(!m_flag_odom || !m_odom.empty()))
    {
        double timeRef;
        if(m_flag_laser) timeRef=m_laser.front().header.stamp.toSec();
        else if(m_flag_odom) timeRef=m_odom.front().header.stamp.toSec();
        else break; //NO SENSOR ON
        if(!m_flag_laser)
        {
            saslam::sa_laser tmp;
            tmp.header.stamp.fromSec(timeRef);
            m_laser.push_back(tmp);
        }
        if(!m_flag_odom)
        {
            nav_msgs::Odometry tmp;
            tmp.header.stamp.fromSec(timeRef);
            m_odom.push_back(tmp);
        }

        std::vector<double> times{m_laser.front().header.stamp.toSec(),m_odom.front().header.stamp.toSec()};
        auto idxMM = std::minmax_element(times.begin(),times.end());
        if(*idxMM.second - *idxMM.first > m_param_timeSync)
        {
            if(idxMM.first-times.begin()==0) m_laser.erase(m_laser.begin());
            else if(idxMM.first-times.begin()==1) m_odom.erase(m_odom.begin());
        }
        else
        {
            if(m_flag_odom)
            {
                Eigen::Matrix4f odom_now = cvt::geoPose2eigen(m_odom.front().pose.pose);
                Eigen::Matrix4f odom_diff = m_odom_before.inverse() * odom_now;
                Eigen::VectorXf odom_diff_xyzrpy = cvt::eigen2xyzrpy(odom_diff);
//                if((sqrt(pow(odom_diff_xyzrpy[0],2)+pow(odom_diff_xyzrpy[1],2)+pow(odom_diff_xyzrpy[2],2)) < m_param_nodeDist &&
//                    fabs(odom_diff_xyzrpy[5]) < m_param_nodeAngle / 180.0 * M_PI))
//                {
//                    m_laser.erase(m_laser.begin());
//                    m_odom.erase(m_odom.begin());
//                    return false;
//                }
                m_odom_before = odom_now;
            }
            nodeOut.idx = m_node_idx++;
            nodeOut.cloud = m_laser.front();
            pcl::PointCloud<pcl::PointXYZ> tmp_ptcl = cvt::cloudmsg2cloud<pcl::PointXYZ>(nodeOut.cloud.laser);
            nodeOut.cloud.laser = cvt::cloud2msg(cvt::cloud2cloudcut(tmp_ptcl,pcl::PointXYZ(0,0,0),m_param_minLaser,m_param_maxLaser));
            m_pub_debug_ptcl.publish(nodeOut.cloud.laser);
            nodeOut.odomPose = m_odom.front().pose.pose;
            nodeOut.slamPose = m_odom.front().pose.pose;
            nodeOut.header = m_odom.front().header;
            m_laser.erase(m_laser.begin());
            m_odom.erase(m_odom.begin());
            return true;
        }
    }
    return false;
}



void nodegen::getparam()
{
    m_nh.param<float>("node/minDist",m_param_nodeDist,0.1);
    m_nh.param<float>("node/minAngle",m_param_nodeAngle,15);
    m_nh.param<float>("sensorInfo/laser/minLaser",m_param_minLaser,0.2);
    m_nh.param<float>("sensorInfo/laser/maxLaser",m_param_maxLaser,20);
    m_nh.param<bool>("sensorOn/odom",m_flag_odom,false);
    m_nh.param<bool>("sensorOn/laser",m_flag_laser,false);
    m_nh.param<float>("node/timeSync",m_param_timeSync,0.01);
}
