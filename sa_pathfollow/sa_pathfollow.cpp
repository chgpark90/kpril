#include <ros/ros.h>
#include "src/pathfollow.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "sa_pathfollow");
  ros::NodeHandle n;
  saslam::pathfollow pathplan_class = saslam::pathfollow();
  ros::spin();
  return 0;
}
