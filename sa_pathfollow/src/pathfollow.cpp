#include "pathfollow.h"

#define _USE_MATH_DEFINES

using namespace saslam;

pathfollow::pathfollow()
{
  getParam();
  m_pub_ctrl = m_nh.advertise<geometry_msgs::Twist>("saslam/pathfollow/ctrl",100);

  static ros::Timer timer1 = m_nh.createTimer(ros::Duration(m_velPublishTime), &pathfollow::spinTimer,this);
  static ros::Subscriber sub1 = m_nh.subscribe("/saslam/pathfollow/waypoint", 1 ,&pathfollow::callback_waypt,this);
}

pathfollow::~pathfollow()
{

}

void pathfollow::getParam()
{
  m_nh_priv = ros::NodeHandle("~");
  m_nh_priv.param("velocity_pubtime",m_velPublishTime,0.1);
  m_nh_priv.param("turn_radius",m_turnRadius,20.0);
  m_nh_priv.param("max_turnspeed",m_maxTurnSpeed,30.0);
  m_nh_priv.param("max_speed",m_maxSpeed,0.2);

}

void pathfollow::spinTimer(const ros::TimerEvent &)
{
  m_pub_ctrl.publish(m_velMsg);
}



void pathfollow::callback_waypt(geometry_msgs::Point::ConstPtr msg)
{
  float angleDiff = atan2(msg->y,msg->x) * 180.0 / M_PI;
  float turnSpeed = angleDiff / m_turnRadius * m_maxTurnSpeed;
  if(fabs(turnSpeed) > m_maxTurnSpeed) turnSpeed = m_maxTurnSpeed*turnSpeed/fabs(turnSpeed);
  float forwardSpeed = 0;
  if(msg->x>0)
  {
    forwardSpeed = m_maxSpeed * (1 - abs(angleDiff)/m_turnRadius);
    if(forwardSpeed<0) forwardSpeed = 0;
  }
  m_velMsg.linear.x = forwardSpeed;
  m_velMsg.angular.z = turnSpeed / 180.0 * M_PI;
}
