#ifndef PATHPLAN_H
#define PATHPLAN_H
#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>

namespace saslam
{
class pathfollow
{
private:
  ros::NodeHandle m_nh;
  ros::NodeHandle m_nh_priv;

  double m_velPublishTime;
  double m_turnRadius;
  double m_maxTurnSpeed;
  double m_maxSpeed;
  geometry_msgs::Twist m_velMsg;
  void getParam();

  ros::Publisher m_pub_ctrl; //For pathfollowing
  void callback_waypt(geometry_msgs::Point::ConstPtr msg);
  void spinTimer(const ros::TimerEvent&);

public:
  pathfollow();
  ~pathfollow();
};
}

#endif

