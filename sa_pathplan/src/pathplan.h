#ifndef PATHPLAN_H
#define PATHPLAN_H


#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/Marker.h>
#include <cmath>

#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include "unavlib/convt.h"
#include "unavlib/others.h"
#include "saslam/sa_mcl.h"
#include "planner/ShortestPP.h"


namespace saslam
{
class pathplan
{

    struct subPath{
        std::vector<Eigen::Vector2f> splinedPath;
        std::vector<Eigen::Vector2f> rawPath;
    };
    struct occumapCv{
        cv::Mat cvMapForPlan;
        cv::Mat cvMapForCheck;
        double resolution;
        Eigen::MatrixXf m_T_global2gridmap;
    };
    struct robotParam{
        double robotSize;
        double robotMargin;
    };

private:
    ros::NodeHandle m_nh;
    ros::NodeHandle m_nh_priv;
    Eigen::MatrixXf m_curPose;
    std::vector<std::pair<geometry_msgs::Point,subPath>> m_waypts;

    std::string m_param_mapdir;
    std::string m_param_updated_map;
    nav_msgs::OccupancyGrid m_occumap;
    occumapCv m_occumapCv;
    robotParam m_robotParam;
    double m_successDistance;
    double m_safeCheckLength;
    double m_safeAreaMargin;
    double m_safeStopTime;

    bool m_flag_stop;
    bool m_flag_stop_continued;
    ros::Time m_stopTime;
    void getParam();

    ros::Publisher m_pub_recv_updated_map;
    ros::Publisher m_pub_base_grid;/**< Node publisher */
    ros::Publisher m_pub_nextWaypt; //For pathfollowing
    ros::Publisher m_pub_wayPt;
    ros::Publisher m_pub_splinedWayPt;
    void callback_crtState(saslam::sa_mcl::ConstPtr msg);
    void callback_goal(geometry_msgs::PoseStamped::ConstPtr msg);
    void callback_waypt(geometry_msgs::PointStamped::ConstPtr msg);
    void callback_updated_map(std_msgs::Bool::ConstPtr msg);
    void spinTimer(const ros::TimerEvent&);

    void readMap(std::string path);
    void pubBase();
    void procMap();

    subPath pathplanning(cv::Mat map, cv::Point src, cv::Point dst);
    float distancePoint(Eigen::MatrixXf pose,Eigen::Vector2f wayPt);
    void clearWaypt();

public:
    pathplan();
    ~pathplan();
};
}

#endif

