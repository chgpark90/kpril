#include "pathplan.h"

#define _USE_MATH_DEFINES

using namespace saslam;
using namespace unavlib;

pathplan::pathplan()
{
  m_flag_stop = false;
  m_flag_stop_continued = false;
  m_pub_recv_updated_map = m_nh.advertise<std_msgs::Bool>("/saslam/pathplan/updated_map_flag", 10);
  m_pub_nextWaypt = m_nh.advertise<geometry_msgs::Point>("saslam/pathplan/nextWaypt",10);
  m_pub_wayPt = m_nh.advertise<sensor_msgs::PointCloud2>("saslam/pathplan/wayPoint",10);
  m_pub_splinedWayPt = m_nh.advertise<nav_msgs::Path>("saslam/pathplan/splinedWayPoint",10);

  static ros::Subscriber sub1 = m_nh.subscribe("/saslam/pathplan/crtState", 10 ,&pathplan::callback_crtState,this);
  static ros::Subscriber sub2 = m_nh.subscribe("/saslam/pathplan/goal", 10 ,&pathplan::callback_goal,this);
  static ros::Subscriber sub3 = m_nh.subscribe("/saslam/pathplan/waypoint", 10 ,&pathplan::callback_waypt,this);
  static ros::Subscriber sub4 = m_nh.subscribe("/saslam/pathplan/updated_map", 10 ,&pathplan::callback_updated_map,this);


  m_pub_base_grid = m_nh.advertise<nav_msgs::OccupancyGrid>("saslam/pathplan/base/grid",10);

  getParam();
  readMap(m_param_mapdir);
  pubBase();
  procMap();

  m_stopTime = ros::Time::now();
  static ros::Timer timer1 = m_nh.createTimer(ros::Duration(0.1), &pathplan::spinTimer,this);
}

pathplan::~pathplan()
{

}


void pathplan::spinTimer(const ros::TimerEvent &)
{
  static double timeBefore = ros::Time::now().toSec();
  if(ros::Time::now().toSec()-timeBefore>10)
  {
    timeBefore = ros::Time::now().toSec();
    pubBase();
  }
  bool flag_visualizeTraj = true;
  if(m_waypts.empty()) flag_visualizeTraj = false;
  else if(m_waypts.front().second.splinedPath.empty()) flag_visualizeTraj = false;
  geometry_msgs::Point targetMsg;
  nav_msgs::Path pathMsg;
  pathMsg.header.frame_id = "map";

  // Visualize trajectory check or Stop check
  if(!flag_visualizeTraj || m_flag_stop) //Publish target point (next waypoint) For stop
  {
    targetMsg.x = 0;targetMsg.y = 0;targetMsg.z = 0; // if all 0, target point is itself.
    m_pub_nextWaypt.publish(targetMsg);
    m_pub_splinedWayPt.publish(pathMsg);
    return;
  }
  int cnt_seq = 0;
  geometry_msgs::PoseStamped tmp_pose;
  tmp_pose.pose.orientation.w = 1;

  //Add current pose for first segment of line
  tmp_pose.pose.position.x = m_curPose(0,3);
  tmp_pose.pose.position.y = m_curPose(1,3);
  tmp_pose.header.seq = cnt_seq++;
  pathMsg.poses.push_back(tmp_pose);

  //Add sub trajectories on line.
  for(int i=0;i<m_waypts.front().second.splinedPath.size();i++)
  {
    geometry_msgs::PoseStamped tmp_pose;
    Eigen::Vector4f pathOnGlobal;
    pathOnGlobal<<m_waypts.front().second.splinedPath.at(i)(0),m_waypts.front().second.splinedPath.at(i)(1),0,1; // Pixel location of path
    pathOnGlobal = m_occumapCv.m_T_global2gridmap.inverse()*pathOnGlobal; // Convert to global location of path
    if(i==0) // Publish pathfollow signal
    {
      Eigen::Vector4f relPathOnGlobal = m_curPose.inverse() * pathOnGlobal;
      targetMsg.x = relPathOnGlobal(0);
      targetMsg.y = relPathOnGlobal(1);
      m_pub_nextWaypt.publish(targetMsg);
    }

    tmp_pose.pose.position.x = pathOnGlobal(0);
    tmp_pose.pose.position.y = pathOnGlobal(1);
    tmp_pose.header.seq = cnt_seq++;
    pathMsg.poses.push_back(tmp_pose);
  }
  m_pub_splinedWayPt.publish(pathMsg); // for visualizing splined

  pcl::PointCloud<pcl::PointXYZRGB> wayptPtcl;
  for(int i=0;i<m_waypts.size();i++)
  {
    cv::Vec3b color = datahandle3d::heightcolor(i/(float)m_waypts.size());
    pcl::PointXYZRGB oneWaypt = pcl::PointXYZRGB(color(0), color(1), color(2));
    Eigen::MatrixXf oneWayptOnMap = cvt::geoPoint2eigen(m_waypts.at(i).first);
    oneWaypt.x = oneWayptOnMap(0);
    oneWaypt.y = oneWayptOnMap(1);
    oneWaypt.z = oneWayptOnMap(2);
    wayptPtcl.push_back(oneWaypt);
  }
  m_pub_wayPt.publish(cvt::cloud2msg(wayptPtcl,"map")); // for visualizing waypoint
}

pathplan::subPath pathplan::pathplanning(cv::Mat map, cv::Point src, cv::Point dst)
{
  subPath resultPP;
  CShortestPP cshortpp;
  iPoint srcPos(src.x,src.y);
  iPoint dstPos(dst.x,dst.y);
  iPointArray arrPath(1000);
  iPointArray pathArrayReal(1000);
  std::cout<<"[PATHPLAN] Searching pass.."<<std::endl;
  cshortpp.FindPathPyr(&map, map.cols, map.cols, map.rows, srcPos, dstPos, arrPath, pathArrayReal);
  std::cout<<"[PATHPLAN] Path found"<<std::endl;
  if(arrPath.size()>1)
  {
    for(int i = 1; i < arrPath.size(); i++)
    {
      resultPP.splinedPath.push_back(Eigen::Vector2f(arrPath[i].x,arrPath[i].y));
    }

    for(int j = 1; j < pathArrayReal.size(); j++)
    {
      resultPP.rawPath.push_back(Eigen::Vector2f(pathArrayReal[j].x,pathArrayReal[j].y));
    }
  }

  cshortpp.~CShortestPP();
  return resultPP;
}

void pathplan::callback_crtState(saslam::sa_mcl::ConstPtr msg)
{
  bool flag_rePlaning = false;

  if(m_occumap.data.size()==0)
  {
    //    std::cout << "[PATHPLAN] Warning : no map " << std::endl;
    return;
  }
  if(m_waypts.empty())
  {
    //        std::cout << "[PATHPLAN] Warning : no waypoint " << std::endl;
    return;
  }
  cv::Mat cvMapForPlan = m_occumapCv.cvMapForPlan.clone();
  Eigen::MatrixXf curPose = cvt::geoPose2eigen(msg->mclPose.pose.pose);
  Eigen::MatrixXf curPoseOnGrid = m_occumapCv.m_T_global2gridmap * curPose;
  m_curPose = curPose;
  //To make empty space ON robot. (when robot is on the obstacle)
  cv::Point curPoseOnGridCV = cv::Point(curPoseOnGrid(0,3),curPoseOnGrid(1,3));
  cv::circle(cvMapForPlan,curPoseOnGridCV,m_robotParam.robotSize/2.0 / m_occumapCv.resolution ,255,-1); //Clear robot pose (prevent block)
  //Transform lidar data into gridmap plane
  pcl::PointCloud<pcl::PointXYZ> lidarScan = cvt::cloudmsg2cloud<pcl::PointXYZ>(msg->lidar);
  pcl::transformPointCloud(lidarScan,lidarScan,curPose);

  if(m_flag_stop&&ros::Time::now().toSec()-m_stopTime.toSec()>m_safeStopTime)
  {
    //AFTER SOME SECONDS,CHECK CURRENT STATUS..
    m_flag_stop = false;
    m_flag_stop_continued = true;
  }
  else if(m_flag_stop)
  {
    return;
  }

  //CHECK OBSTACLES ON PATH
  if(!m_waypts.empty() && !m_waypts.front().second.splinedPath.empty())
  {
    bool flag_obstacleCheck = false;
    cv::Mat mapPathCheck;
    cv::cvtColor(m_occumapCv.cvMapForCheck,mapPathCheck,cv::COLOR_GRAY2BGR);
    cv::Mat mapIfObstacle = cvMapForPlan.clone();
    for(int i=0;i<lidarScan.size();i++)
    {
      pcl::PointXYZ point_temp = lidarScan.at(i);
      geometry_msgs::Point ptTmp;
      ptTmp.x = point_temp.x; ptTmp.y = point_temp.y; ptTmp.z = 0;
      Eigen::MatrixXf curPoint = cvt::geoPoint2eigen(ptTmp);
      Eigen::MatrixXf curPointOnGrid = m_occumapCv.m_T_global2gridmap * curPoint;
      cv::Point curPointOnGridCV = cv::Point(curPointOnGrid(0,0),curPointOnGrid(1,0));
      try
      {
        cv::circle(mapPathCheck,curPointOnGridCV,(m_robotParam.robotSize/2.0) / m_occumapCv.resolution ,
                   cv::Scalar(255,0,0),-1);
        cv::circle(mapIfObstacle,curPointOnGridCV,(m_robotParam.robotSize/2.0+m_safeAreaMargin) / m_occumapCv.resolution ,0,-1);
      }
      catch (cv::Exception& e)
      {
      }
    }
    cv::circle(mapIfObstacle,curPoseOnGridCV,m_robotParam.robotSize/2.0 / m_occumapCv.resolution ,255,-1); //Clear robot pose (prevent block)


    double checkedLength = 0;
    for(int i=0;i<m_waypts.front().second.splinedPath.size()-1;i++)
    {
      cv::Point pt1;
      if(i==0) pt1 = curPoseOnGridCV;
      else pt1 = cv::Point(m_waypts.front().second.splinedPath[i](0),m_waypts.front().second.splinedPath[i](1));
          cv::Point pt2(m_waypts.front().second.splinedPath[i+1](0),m_waypts.front().second.splinedPath[i+1](1));
      cv::LineIterator it(mapPathCheck, pt1, pt2, 8);
      cv::Point posBefore;
      for(int i = 0; i < it.count; i++, ++it)
      {
        cv::Vec3b val = mapPathCheck.at<cv::Vec3b>(it.pos());
        if(i!=0) checkedLength += cv::norm(it.pos()-posBefore) * m_occumapCv.resolution;
        if(val==cv::Vec3b(255,0,0))
        {
          flag_obstacleCheck = true;
          break;
        }
        else if(checkedLength>m_safeCheckLength)
        {
          break;
        }
//        cv::circle(mapPathCheck,it.pos(),1 ,  cv::Scalar(0,0,255),-1);
        posBefore = it.pos();
      }
      if(flag_obstacleCheck || checkedLength>m_safeCheckLength) break;
    }
    if(flag_obstacleCheck) //IF THERE ARE LIDAR POINTS IN SAFE AREA
    {
      std::cout<<"[PATHPLAN] OBSTACLE IN PATH : "<<checkedLength<<"[m]"<<std::endl;
      if(m_flag_stop_continued) //IF THERE ARE OBSTACLE REMAINED.
      {
        m_flag_stop_continued = false;
        std::cout<<"[PATHPLAN] REPLANNING WITH ANOTHER ROUTE"<<std::endl;
        m_waypts.front().second.splinedPath.clear(); //REMOVE PRE-CALCULATED PATH
        cvMapForPlan = mapIfObstacle.clone();
      }
      else //FIRST DETECTED OBJECT
      {
        std::cout<<"[PATHPLAN] WAITING OBSTACLE FOR "<<m_safeStopTime<<" Seconds.."<<std::endl;
        m_flag_stop = true;
        m_stopTime = ros::Time::now();
        return;
      }
    }
    else if(m_flag_stop_continued) //OBSTACLE REMOVED AFTER SOME SECODS
    {
      std::cout<<"[PATHPLAN] OBSTACLE REMOVED! JUST FOLLOW PREVIOUS PATH"<<std::endl;
      m_flag_stop_continued = false;
    }
  }


  if(m_waypts.front().second.splinedPath.empty()) flag_rePlaning = true; //When no subpath exist
  else //Subpath exist
  {
    float distance = distancePoint(curPoseOnGrid,m_waypts.front().second.splinedPath.front()); //Calc distance in pixel
    distance = distance * m_occumapCv.resolution; //Convert to [m] unit
    if(distance < m_successDistance) //If robot is at waypoint
    {
      m_waypts.front().second.splinedPath.erase(m_waypts.front().second.splinedPath.begin());
      if(m_waypts.front().second.splinedPath.empty())
      {
        m_waypts.erase(m_waypts.begin());
        return;
      }
    }
  }
  if(flag_rePlaning) // NOT YET PATH PLANNED
  {
    Eigen::MatrixXf goalOnGrid = m_occumapCv.m_T_global2gridmap * cvt::geoPoint2eigen(m_waypts.front().first);
    cv::Point goalOnGridCV = cv::Point(goalOnGrid(0,0),goalOnGrid(1,0));
    m_waypts.front().second = pathplanning(cvMapForPlan,curPoseOnGridCV,goalOnGridCV); // Generate Path
    std::cout << "[PATHPLAN] Subpath quantity : "<<m_waypts.front().second.splinedPath.size() << std::endl;

    if(m_waypts.front().second.splinedPath.size()==0)
    {
      std::cout << "[PATHPLAN] Warning : Cannot found path. Erase current path" << std::endl;
      m_waypts.erase(m_waypts.begin());
      return;
    }
  }
}

float pathplan::distancePoint(Eigen::MatrixXf pose,Eigen::Vector2f wayPt)
{
  return sqrt(pow(pose(0,3)-wayPt(0),2)+pow(pose(1,3)-wayPt(1),2));
}

void pathplan::procMap()
{ 
  float resolution = m_occumap.info.resolution;
  cv::Mat mapIn = cvt::occumap2cvimg(m_occumap);
  std::cout<<"MAP SIZE : "<<mapIn.size().height<<","<<mapIn.size().width<<std::endl;
  if(mapIn.size().height>2000 || mapIn.size().width>2000)
  {
    float reductionRatio = std::max(mapIn.size().height,mapIn.size().width) / 2000.0;
    resolution *= reductionRatio;
    cv::resize(mapIn,mapIn,cv::Size(mapIn.size().width/reductionRatio,mapIn.size().height/reductionRatio));
  }
  std::cout<<"MAP SIZE : "<<mapIn.size().height<<","<<mapIn.size().width<<std::endl;

  m_occumapCv.resolution = resolution;
  m_occumapCv.m_T_global2gridmap = Eigen::Matrix4f::Identity();
  m_occumapCv.m_T_global2gridmap(0,0) = 1./resolution;
  m_occumapCv.m_T_global2gridmap(1,1) = 1./resolution;
  m_occumapCv.m_T_global2gridmap(0,3) = -m_occumap.info.origin.position.x / resolution;
  m_occumapCv.m_T_global2gridmap(1,3) = -m_occumap.info.origin.position.y / resolution;

  cv::Mat element_dil = cv::getStructuringElement( cv::MORPH_ELLIPSE, cv::Size(3,3), cv::Point(2,2));
  cv::dilate(mapIn,mapIn,element_dil);
  cv::erode( mapIn,mapIn,element_dil);

  cv::Mat element_ero = cv::getStructuringElement( cv::MORPH_ELLIPSE,
                                                   cv::Size( (m_robotParam.robotSize - m_safeAreaMargin*2) / resolution + 1,
                                                             (m_robotParam.robotSize - m_safeAreaMargin*2) / resolution + 1 ),
                                                   cv::Point( (m_robotParam.robotSize - m_safeAreaMargin*2) / resolution,
                                                              (m_robotParam.robotSize - m_safeAreaMargin*2) / resolution ) );
  cv::erode( mapIn, m_occumapCv.cvMapForCheck, element_ero );

  element_ero = cv::getStructuringElement( cv::MORPH_ELLIPSE,
                                           cv::Size( m_robotParam.robotSize / resolution + 1,
                                                     m_robotParam.robotSize / resolution + 1 ),
                                           cv::Point( m_robotParam.robotSize /resolution,
                                                      m_robotParam.robotSize / resolution ) );
  //cv::erode( mapIn, m_occumapCv.cvMapForPlan, element_ero );
  cv::erode( mapIn, mapIn, element_ero );
  m_occumapCv.cvMapForPlan = mapIn.clone();
}

void pathplan::callback_goal(geometry_msgs::PoseStamped::ConstPtr msg)
{
  clearWaypt();
  subPath subWaypts;
  m_waypts.push_back(std::make_pair(msg->pose.position,subWaypts));
}

void pathplan::callback_waypt(geometry_msgs::PointStamped::ConstPtr msg)
{
  subPath subWaypts;
  m_waypts.push_back(std::make_pair(msg->point,subWaypts));
}

void pathplan::callback_updated_map(std_msgs::Bool::ConstPtr msg)
{
  // occumap update
  readMap(m_param_updated_map);
  procMap();
  std_msgs::Bool tmp;
  tmp.data = true;
  m_pub_recv_updated_map.publish(tmp);
  subPath subWaypts;
  m_waypts.front().second = subWaypts;
  pubBase();
}

void pathplan::clearWaypt()
{
  m_waypts.clear();
}


void pathplan::getParam()
{
  m_nh.param("movingControl/robot_size",m_robotParam.robotSize,1.0);
  m_nh.param("movingControl/robot_margin",m_robotParam.robotMargin,1.0);
  m_nh.param("movingControl/success_distance",m_successDistance,1.0);
  m_nh.param("movingControl/safearea_length",m_safeCheckLength,0.2);
  m_nh.param("movingControl/safearea_margin",m_safeAreaMargin,0.2);
  m_nh.param("movingControl/safestop_time",m_safeStopTime,5.0);

  m_nh.param<std::string>("movingControl/objectmapPath",m_param_mapdir,"~/.ros");
  m_nh.param<std::string>("movingControl/updatedmapPath",m_param_updated_map,"~/.ros");
}

void pathplan::pubBase()
{
  m_pub_base_grid.publish(m_occumap);
}

void pathplan::readMap(std::string path)
{
  std::stringstream sstmG;
  sstmG<<path<<"/occumap";
  std::cout<<"[PATHPLAN] LOAD OCCU MAP : "<<sstmG.str()<<std::endl;
  cvt::loadOccupancymap(sstmG.str(),m_occumap);
  m_occumap.header.frame_id="map";
}
