#include <ros/ros.h>
#include "src/pathplan.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lm_pathplan");
  ros::NodeHandle n;
  saslam::pathplan pathplan_class = saslam::pathplan();
  ros::spin();
  return 0;
}
