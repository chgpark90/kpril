file(REMOVE_RECURSE
  "CMakeFiles/saslam_generate_messages_cpp"
  "devel/include/saslam/sa_constraint.h"
  "devel/include/saslam/sa_icpQuery.h"
  "devel/include/saslam/sa_icpResult.h"
  "devel/include/saslam/sa_laser.h"
  "devel/include/saslam/sa_mcl.h"
  "devel/include/saslam/sa_node.h"
  "devel/include/saslam/sa_optimizeInfo.h"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/saslam_generate_messages_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
