file(REMOVE_RECURSE
  "CMakeFiles/saslam_generate_messages_nodejs"
  "devel/share/gennodejs/ros/saslam/msg/sa_constraint.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_icpQuery.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_icpResult.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_laser.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_mcl.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_node.js"
  "devel/share/gennodejs/ros/saslam/msg/sa_optimizeInfo.js"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/saslam_generate_messages_nodejs.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
