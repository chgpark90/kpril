set(_CATKIN_CURRENT_PACKAGE "saslam")
set(saslam_VERSION "0.0.1")
set(saslam_MAINTAINER "Seungwon Song <sswan55@kaist.ac.kr>")
set(saslam_PACKAGE_FORMAT "1")
set(saslam_BUILD_DEPENDS "roscpp" "std_msgs" "rospy" "geometry_msgs" "nav_msgs" "sensor_msgs" "libpcl-all")
set(saslam_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs" "rospy" "geometry_msgs" "nav_msgs" "sensor_msgs" "libpcl-all")
set(saslam_BUILDTOOL_DEPENDS "catkin")
set(saslam_BUILDTOOL_EXPORT_DEPENDS )
set(saslam_EXEC_DEPENDS "roscpp" "std_msgs" "rospy" "geometry_msgs" "nav_msgs" "sensor_msgs" "libpcl-all")
set(saslam_RUN_DEPENDS "roscpp" "std_msgs" "rospy" "geometry_msgs" "nav_msgs" "sensor_msgs" "libpcl-all")
set(saslam_TEST_DEPENDS )
set(saslam_DOC_DEPENDS )
set(saslam_URL_WEBSITE "")
set(saslam_URL_BUGTRACKER "")
set(saslam_URL_REPOSITORY "")
set(saslam_DEPRECATED "")