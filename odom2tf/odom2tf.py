#!/usr/bin/env python  
import rospy
import tf
import tf.msg
from nav_msgs.msg import Odometry
import geometry_msgs.msg
import math
import sys

global_t = geometry_msgs.msg.TransformStamped();

class DynamicTFBroadcaster:

    def __init__(self):
        self.pub_tf = rospy.Publisher("/tf", tf.msg.tfMessage,queue_size=50)

        change = 0.0
        while not rospy.is_shutdown():
            # Run this loop at about 10Hz
            rospy.sleep(0.1)
            tfm = tf.msg.tfMessage([global_t])
            self.pub_tf.publish(tfm)

def odometryCb(msg):
    global_t.header.frame_id = msg.header.frame_id
    global_t.header.seq = msg.header.seq
    global_t.header.stamp = msg.header.stamp
    global_t.child_frame_id = msg.child_frame_id
    global_t.transform.translation.x = msg.pose.pose.position.x
    global_t.transform.translation.y = msg.pose.pose.position.y
    global_t.transform.translation.z = msg.pose.pose.position.z

    global_t.transform.rotation.x = msg.pose.pose.orientation.x
    global_t.transform.rotation.y = msg.pose.pose.orientation.y
    global_t.transform.rotation.z = msg.pose.pose.orientation.z
    global_t.transform.rotation.w = msg.pose.pose.orientation.w

    


if __name__ == '__main__':
    if(len(sys.argv)!=1):
        print("Please insert topic name")
    rospy.init_node('my_tf_broadcaster')
    rospy.Subscriber('/odom',Odometry,odometryCb)
    tfb = DynamicTFBroadcaster()
    rospy.spin()
