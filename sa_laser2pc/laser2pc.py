#!/usr/bin/env python
import rospy
from sensor_msgs.msg import PointCloud2 as pc2
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose
from laser_geometry import LaserProjection
from saslam.msg import sa_laser



class Laser2PC():
    def __init__(self):
        self.laserProj = LaserProjection()
        self.outMsg = rospy.get_param('~outMsg')
        self.laserSub = rospy.Subscriber(rospy.get_param('topicName/laser')[0],LaserScan,self.laserCallback)
        laserParam = rospy.get_param('tf/laser')[0]
        laserP = map(float, laserParam.split())
        self.laserPose = Pose()
        self.laserPose.position.x, self.laserPose.position.y, self.laserPose.position.z = laserP[0:3]
        self.laserPose.orientation.x, self.laserPose.orientation.y, self.laserPose.orientation.z, self.laserPose.orientation.w = laserP[3:7]
        self.laserPub = rospy.Publisher(self.outMsg,sa_laser,queue_size=10)


    def laserCallback(self,data):
        laser_out = sa_laser()
        laser_out.header = data.header
        laser_out.laser = self.laserProj.projectLaser(data)
        laser_out.T_laser2rt = self.laserPose
        self.laserPub.publish(laser_out)


if __name__=='__main__':
    rospy.init_node("laser2PointCloud")
    l2pc = Laser2PC()
    rospy.spin()

