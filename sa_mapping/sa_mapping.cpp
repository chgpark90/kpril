#include <ros/ros.h>
#include "src/mapping.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mm_mapping");
    saslam::mapping mapping_c = saslam::mapping();
    ros::spin();

    return 0;
}
