#include "mapping.h"

using namespace saslam;
using namespace unavlib;

mapping::mapping()
{
  getparam();

  m_sub_map = m_nh.subscribe("/map",1,&mapping::callback_save_map,this);
}

mapping::~mapping()
{

}

void mapping::getparam()
{
    m_nh.param<std::string>("movingControl/objectmapPath",m_param_mapdir,"~/.ros");
}

void mapping::callback_save_map(const nav_msgs::OccupancyGrid::ConstPtr &msg)
{
  m_occumap.data = msg->data;
  m_occumap.header = msg->header;
  m_occumap.info = msg->info;
  saveMap(m_param_mapdir);
}

void mapping::saveMap(std::string path)
{
  std::stringstream sstmG;
  sstmG<<path<<"/occumap";
  std::cout<<"[MAPPING] SAVE OCCU MAP : "<<sstmG.str()<<std::endl;
  cvt::saveOccupanymap(sstmG.str(), m_occumap);
}
