#ifndef MAPPING_H
#define MAPPING_H
#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/MarkerArray.h>

#include "unavlib/convt.h"
#include "unavlib/others.h"



namespace saslam
{
    class mapping
    {
    private:
      ros::NodeHandle m_nh;
      ros::Subscriber m_sub_map; /**< Node for save data to pose_vec */

      std::string m_param_mapdir;
      nav_msgs::OccupancyGrid m_occumap;

      void getparam(); /**< To get parameter from launch file */

      void callback_save_map(const nav_msgs::OccupancyGrid::ConstPtr &msg); /**< Node subscribe */
      void saveMap(std::string path);


    public:
      mapping();
      ~mapping();
    };

}


#endif

