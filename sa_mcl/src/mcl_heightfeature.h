#ifndef MCL_MANAGER_H
#define MCL_MANAGER_H

#include "mcl_base.h"
#include "unavlib/convt.h"
#include "nav_msgs/OccupancyGrid.h"



namespace saslam
{
  struct gridmap{
    cv::Mat map;
    Eigen::Vector3f originPose;
    float resolution;
  };

  class mcl_heightfeature : public mcl_base
  {
  private:

    Eigen::VectorXf m_param_OdomModelCov;

  protected:
    bool isOnmap(Eigen::MatrixXf pose);
    float calcWeight(pcl::PointXYZ pt);
    std::vector<particle> prediction(std::vector<particle> particles, Eigen::MatrixXf trans);
    pcl::PointCloud<pcl::PointXYZ> featureExtract(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

  public:
      mcl_heightfeature();
      ~mcl_heightfeature();

      gridmap m_gridmap;

      inline void setOdomModel(Eigen::MatrixXf odommodel){m_param_OdomModelCov = odommodel;}
      pcl::PointCloud<pcl::PointXYZ> updateGridmap(void* map);




  };

}


#endif

