#include "mcl_base.h"

using namespace saslam;
using namespace unavlib;


mcl_base::mcl_base() :
  m_particles(),
  m_featurePts(),
  m_predictPose(Eigen::Matrix4f::Identity())
{
  m_maxNumParticle = 10000;
  m_minNumParticle = 1000;

  initParticle(Eigen::Matrix4f::Identity(),2,180);
}


mcl_base::~mcl_base()
{

}

void mcl_base::initParticle(Eigen::Matrix4f initpose, float boundary, float boundary_angle)
{
  Eigen::VectorXf pos_in = cvt::eigen2xyzrpy(initpose);

  std::default_random_engine gen;
  std::uniform_real_distribution<double> x_pos(pos_in[0] - boundary, pos_in[0] + boundary);
  std::uniform_real_distribution<double> y_pos(pos_in[1] - boundary, pos_in[1] + boundary);
  std::uniform_real_distribution<double> theta_pos(pos_in[5] - boundary_angle * M_PI / 180, pos_in[5] + boundary_angle * M_PI / 180);
  m_particles.clear();
  for(int i=0;i<m_maxNumParticle;i++)
  {
      particle particle_temp;
      particle_temp.pose = cvt::xyzrpy2eigen(x_pos(gen),y_pos(gen),0,0,0,theta_pos(gen));
      particle_temp.score = 1/(float)m_maxNumParticle;
      m_particles.push_back(particle_temp);
  }
}

particle mcl_base::spinOnce()
{

  m_particles = prediction(m_particles, m_predictPose);
  weightning();  
  particle bestParticle = getBestParticle();
  resampling();
  return bestParticle;
}

void mcl_base::weightning()
{
  for(int i=0;i<m_particles.size();i++)
  {
    pcl::PointCloud<pcl::PointXYZ> lidar_now;
    pcl::transformPointCloud(m_featurePts,lidar_now,m_particles.at(i).pose);
    float weight = 0;
    for(int j=0;j<lidar_now.size();j++)
    {
        pcl::PointXYZ lidar_dot = lidar_now.at(j);
        weight += calcWeight(lidar_dot);
    }
    weight = weight /lidar_now.size() + 0.00000000001;

    m_particles.at(i).score = m_particles.at(i).score * weight;
  }
}

void mcl_base::resampling()
{
  std::default_random_engine gen;

  float particles_sumscore = 0;
  for(int i=0;i<m_particles.size();i++)
  {
      particles_sumscore+= m_particles.at(i).score;
  }
  std::vector<float> particles_scores;
  float normalized_scores = 0;
  for(int i=0;i<m_particles.size();i++){
      m_particles.at(i).score = m_particles.at(i).score / particles_sumscore; // This is for preventing specific particle to have too much score.
      normalized_scores += m_particles.at(i).score;
      particles_scores.push_back(normalized_scores);
  }
  std::uniform_real_distribution<double> dart(0, particles_scores.back());
  std::vector<particle> particles_sampled;
  int particleNum = adapted_quantity();
  for(int i=0;i<particleNum;i++)
  {
      bool particle_selected = false;
      while(!particle_selected)
      {

          double darted = dart(gen);
          for(int j=0;j<particles_scores.size();j++)
          {
            if(darted<particles_scores.at(j))
            {
              if(!isOnmap(m_particles.at(j).pose))
                break;
              else
              {
                particles_sampled.push_back(m_particles.at(j));
                particle_selected = true;
                break;
              }
            }
          }
      }
  }
  m_particles = particles_sampled;
}

particle mcl_base::getBestParticle()
{
  particle max_particle;
  max_particle.score = std::numeric_limits<float>::min();

  for(int i=0;i<m_particles.size();i++)
  {
      if(m_particles.at(i).score > max_particle.score)
      {
          max_particle = m_particles.at(i);
      }
  }
  return max_particle;
}

int mcl_base::adapted_quantity()
{
    float max_x,max_y,min_x,min_y;
    max_x = std::numeric_limits<float>::min(); max_y = std::numeric_limits<float>::min();
    min_x = std::numeric_limits<float>::max(); min_y = std::numeric_limits<float>::max();
    for(int i=0;i<m_particles.size();i++)
    {
        Eigen::VectorXf pose_xyzrpy = cvt::eigen2xyzrpy(m_particles.at(i).pose);
        if(pose_xyzrpy[0]>max_x) max_x = pose_xyzrpy[0];
        if(pose_xyzrpy[0]<min_x) min_x = pose_xyzrpy[0];
        if(pose_xyzrpy[1]>max_y) max_y = pose_xyzrpy[1];
        if(pose_xyzrpy[1]<min_y) min_y = pose_xyzrpy[1];
    }
    if((fabs(max_x-min_x)*fabs(max_y-min_y)*100)>m_maxNumParticle) return m_maxNumParticle;
    else if((fabs(max_x-min_x)*fabs(max_y-min_y)*100)<(m_minNumParticle)) return (m_minNumParticle);
    else return (fabs(max_x-min_x)*fabs(max_y-min_y)*100);
}
