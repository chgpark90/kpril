#ifndef MCL_H
#define MCL_H
#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/MarkerArray.h>
#include "saslam/sa_node.h"
#include "saslam/sa_mcl.h"


#include "mcl_heightfeature.h"

#include "unavlib/convt.h"
#include "unavlib/others.h"



namespace saslam
{
    class mcl
    {
    private:
      ros::NodeHandle m_nh;
      ros::NodeHandle m_nh_priv;
      ros::Subscriber m_sub_node; /**< Node for save data to pose_vec */
      ros::Subscriber m_sub_initPose;
      ros::Publisher m_pub_mclPose;
      ros::Publisher m_pub_mclLiDAR;
      ros::Publisher m_pub_mclParticles;
      ros::Publisher m_pub_mclInfo;


      std::string m_param_mapdir;
      int m_param_particleMin;
      int m_param_particleMax;
      Eigen::VectorXf m_param_cov;

      mcl_heightfeature m_mcl_heightfeature;
      pcl::PointCloud<pcl::PointXYZ> m_featuremap;
      Eigen::Matrix4f m_bestpose;
      pcl::PointCloud<pcl::PointXYZ> m_cloud_feature;
      nav_msgs::OccupancyGrid m_occumap;



      void getparam(); /**< To get parameter from launch file */
      void pubparticle(particle particleo);
      void publidar(pcl::PointCloud<pcl::PointXYZ> lidar, Eigen::Matrix4f pose);
      void pubparticles(std::vector<particle> particles);

      void callback_timer(const ros::TimerEvent& event);
      void callback_node(const sa_node::ConstPtr &msg); /**< Node subscribe */
      void callback_initPose(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg); /**< Node subscribe */


    public:
      mcl();
      ~mcl();

      void pubMap();
    };

}


#endif

