#ifndef MCL_BASE_H
#define MCL_BASE_H
#include <random>
#include "unavlib/convt.h"
#include "unavlib/others.h"



namespace saslam
{
  struct particle{
      Eigen::Matrix4f pose;
      float score;
  };
  class mcl_base
  {
  private:
    int m_maxNumParticle;
    int m_minNumParticle;


    void resampling();
    particle getBestParticle();
    int adapted_quantity();
    void weightning();

  protected:

    virtual std::vector<particle> prediction(std::vector<particle> particles,  Eigen::MatrixXf trans) {}
    virtual bool isOnmap(Eigen::MatrixXf pose) {return 0;}
    virtual float calcWeight(pcl::PointXYZ pt) {return 0;}
    virtual pcl::PointCloud<pcl::PointXYZ> featureExtract(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud) {}

    std::vector<particle> m_particles;
    //gridmap m_gridmap;
    pcl::PointCloud<pcl::PointXYZ> m_featurePts;
    Eigen::Matrix4f m_predictPose;

  public:
    mcl_base();
    ~mcl_base();

    inline void setNofParticles(int max, int min) {m_maxNumParticle = max; m_minNumParticle = min;}
    virtual pcl::PointCloud<pcl::PointXYZ> updateGridmap(void* map) {}
    inline pcl::PointCloud<pcl::PointXYZ> updateCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud){m_featurePts = featureExtract(cloud); return m_featurePts;}
    inline void updatePredictPose(Eigen::Matrix4f pose){m_predictPose = pose;}
    void initParticle(Eigen::Matrix4f initpose = Eigen::Matrix4f::Identity(), float boundary = 2, float boundary_angle = 5);
    inline std::vector<particle> getParticles(){return m_particles;}
    particle spinOnce();



  };

}


#endif

