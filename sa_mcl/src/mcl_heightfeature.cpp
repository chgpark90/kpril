#include "mcl_heightfeature.h"

using namespace saslam;
using namespace unavlib;

mcl_heightfeature::mcl_heightfeature()
{
  m_param_OdomModelCov = Eigen::VectorXf(6);
  m_param_OdomModelCov[0] = 0.05;
  m_param_OdomModelCov[1] = 0.02;
  m_param_OdomModelCov[2] = 0.02;
  m_param_OdomModelCov[3] = 0.002;
  m_param_OdomModelCov[4] = 0.02;
  m_param_OdomModelCov[5] = 0.005;
}

mcl_heightfeature::~mcl_heightfeature()
{
}

bool mcl_heightfeature::isOnmap(Eigen::MatrixXf pose)
{
  float x = pose(0,3) - m_gridmap.originPose(0);
  float y = pose(1,3) - m_gridmap.originPose(1);
  int x_onMap = x / m_gridmap.resolution;
  int y_onMap = y / m_gridmap.resolution;
  if(x_onMap < 0 || x_onMap >= m_gridmap.map.size().width || y_onMap <0 || y_onMap >= m_gridmap.map.size().height)
    return false;
  else
    return true;
}

pcl::PointCloud<pcl::PointXYZ> mcl_heightfeature::updateGridmap(void* map)
{
  nav_msgs::OccupancyGrid* map_input = (nav_msgs::OccupancyGrid*)map;
  cv::Mat cvimgmap = cvt::occumap2cvimg(*map_input);
  m_gridmap.map = cv::Mat::zeros(cvimgmap.size(), CV_8UC1);
  m_gridmap.originPose(0) = map_input->info.origin.position.x;
  m_gridmap.originPose(1) = map_input->info.origin.position.y;
  m_gridmap.resolution = map_input->info.resolution;

  map_input->info.origin.position.x;
  map_input->info.origin.position.y;


  pcl::PointCloud<pcl::PointXYZ> mapcloud;
  for(int y = 0; y < cvimgmap.size().height; y++)
  {
    for(int x = 0; x < cvimgmap.size().width; x++)
    {
      int data = cvimgmap.at<unsigned char>(y,x);
      if(data < 100)
      {
        m_gridmap.map.at<unsigned char>(y,x) = 255 - data;
        pcl::PointXYZ tmp_pt;
        tmp_pt.x = (float)x * m_gridmap.resolution + m_gridmap.originPose(0);
        tmp_pt.y = (float)y * m_gridmap.resolution + m_gridmap.originPose(1);
        tmp_pt.z = 0;
        mapcloud.push_back(tmp_pt);
      }
    }
  }
  cv::GaussianBlur(m_gridmap.map, m_gridmap.map, cv::Size(21,21),0);
  return mapcloud;
}

float mcl_heightfeature::calcWeight(pcl::PointXYZ pt)
{
  int pt_x = (pt.x - m_gridmap.originPose(0)) / m_gridmap.resolution;
  int pt_y = (pt.y - m_gridmap.originPose(1)) / m_gridmap.resolution;
  if(pt_x<0 || pt_x>=m_gridmap.map.size().width || pt_y<0 ||  pt_y>=m_gridmap.map.size().height)
    return 0;
  else
    return (float)m_gridmap.map.at<uchar>(pt_y,pt_x)/(double)255;
}

pcl::PointCloud<pcl::PointXYZ> mcl_heightfeature::featureExtract(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
  pcl::PointCloud<pcl::PointXYZ> featurePts;
  for(int i = 0; i < cloud->size(); i++)
  {
    //if(cloud->at(i).z > 1 && cloud->at(i).z < 1.5)
    {
      pcl::PointXYZ tmp_xyz;
      tmp_xyz.x = cloud->at(i).x;
      tmp_xyz.y = cloud->at(i).y;
      tmp_xyz.z = 0;
      featurePts.push_back(tmp_xyz);
    }
  }
  return featurePts;
}

std::vector<particle> mcl_heightfeature::prediction(std::vector<particle> particles, Eigen::MatrixXf trans)
{
    Eigen::VectorXf diff_xyzrpy = cvt::eigen2xyzrpy(trans);
    double delta_trans = sqrt(pow(diff_xyzrpy[0],2) + pow(diff_xyzrpy[1],2));
    double delta_rot1 = atan2(diff_xyzrpy[1], diff_xyzrpy[0]);
    if(delta_trans < 0.001)
        delta_rot1 = 0;
    double delta_rot2 = diff_xyzrpy[5] - delta_rot1;

    if(delta_rot1  > M_PI)
        delta_rot1 -= (2*M_PI);
    if(delta_rot1  < -M_PI)
        delta_rot1 += (2*M_PI);
    if(delta_rot2  > M_PI)
        delta_rot2 -= (2*M_PI);
    if(delta_rot2  < -M_PI)
        delta_rot2 += (2*M_PI);

    double delta_trans_err = m_param_OdomModelCov[2] * abs(delta_trans) + m_param_OdomModelCov[3] * abs(delta_rot1 + delta_rot2);
    double delta_rot1_err = m_param_OdomModelCov[0] * abs(delta_rot1) + m_param_OdomModelCov[1] * abs(delta_trans);
    double delta_rot2_err = m_param_OdomModelCov[0] * abs(delta_rot2) + m_param_OdomModelCov[1] * abs(delta_trans);

    for(int i=0;i<particles.size();i++)
    {
        double tilde_trans = delta_trans + probabilistic::GaussianRand() * delta_trans_err;
        double tilde_rot1 = delta_rot1 + probabilistic::GaussianRand() * delta_rot1_err;
        double tilde_rot2 = delta_rot2 + probabilistic::GaussianRand() * delta_rot2_err;

        double tmp_diff_x = tilde_trans * cos(tilde_rot1) + probabilistic::GaussianRand() * m_param_OdomModelCov[4];
        double tmp_diff_y = tilde_trans * sin(tilde_rot1) + probabilistic::GaussianRand() * m_param_OdomModelCov[4];
        double tmp_diff_theta = tilde_rot1 + tilde_rot2 + probabilistic::GaussianRand() * m_param_OdomModelCov[5] * CV_PI / 180;

        particles.at(i).pose= particles.at(i).pose
                * cvt::xyzrpy2eigen(tmp_diff_x,tmp_diff_y,0, 0,0,tmp_diff_theta);
    }
    return particles;
}
