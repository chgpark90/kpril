#include "mcl.h"

using namespace saslam;
using namespace unavlib;

mcl::mcl()
{
  getparam();

  static ros::Subscriber sub1 = m_nh.subscribe<sa_node>("/saslam/mcl/node",1,&mcl::callback_node,this);
  static ros::Subscriber sub2 = m_nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>
          ("/saslam/mcl/initPose",1000,&mcl::callback_initPose,this);
  m_pub_mclPose = m_nh.advertise<nav_msgs::Odometry>("saslam/mcl/pose",1);
  m_pub_mclLiDAR = m_nh.advertise<sensor_msgs::PointCloud2>("saslam/mcl/lidar",1);
  m_pub_mclParticles = m_nh.advertise<sensor_msgs::PointCloud2>("saslam/mcl/particles",1);
  m_pub_mclInfo = m_nh.advertise<sa_mcl>("saslam/mcl/info",1);

  static ros::Timer timer = m_nh.createTimer(ros::Duration(0.1), &mcl::callback_timer,this);

  m_mcl_heightfeature = mcl_heightfeature();
  m_mcl_heightfeature.setNofParticles(m_param_particleMax,m_param_particleMin);
  m_mcl_heightfeature.setOdomModel(m_param_cov);

  m_bestpose = Eigen::Matrix4f::Identity();

  // map handling
  std::stringstream sstmG;
  sstmG<<m_param_mapdir<<"/occumap";
  std::cout<<"[MCL] LOAD MAP : "<<sstmG.str()<<std::endl;
  cvt::loadOccupancymap(sstmG.str(),m_occumap);
  m_occumap.header.frame_id="map";
  m_featuremap = m_mcl_heightfeature.updateGridmap(&m_occumap);
}

mcl::~mcl()
{

}

void mcl::getparam()
{
    m_nh.param<std::string>("movingControl/objectmapPath",m_param_mapdir,"~/.ros");
    m_nh.param("movingControl/particle_min",m_param_particleMin,1000);
    m_nh.param("movingControl/particle_max",m_param_particleMax,10000);
    std::vector<double> covOdom;
    m_param_cov = Eigen::VectorXf(6);
    if(m_nh.getParam("covariance/odom",covOdom))
    {
        if(covOdom.size() == 6)
        {
            m_param_cov[0] = covOdom[0];
            m_param_cov[1] = covOdom[1];
            m_param_cov[2] = covOdom[2];
            m_param_cov[3] = covOdom[3];
            m_param_cov[4] = covOdom[4];
            m_param_cov[5] = covOdom[5];
        }
    }
}

void mcl::callback_timer(const ros::TimerEvent& event)
{
  sa_mcl mclInfo;
  mclInfo.lidar = cvt::cloud2msg(m_cloud_feature);
  mclInfo.mclPose.pose.pose = cvt::eigen2geoPose(m_bestpose);
  m_pub_mclInfo.publish(mclInfo);
}

void mcl::callback_node(const sa_node::ConstPtr &msg)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
  *cloud = cvt::cloudmsg2cloud<pcl::PointXYZ>(msg->cloud.laser);
  pcl::transformPointCloud(*cloud,*cloud,cvt::geoPose2eigen(msg->cloud.T_laser2rt));

  m_cloud_feature = m_mcl_heightfeature.updateCloud(cloud);
  Eigen::Matrix4f crtodom = cvt::geoPose2eigen(msg->odomPose);
  static Eigen::Matrix4f s_preodom = crtodom;
  m_mcl_heightfeature.updatePredictPose(s_preodom.inverse() * crtodom);
  s_preodom = crtodom;
  particle best_particle = m_mcl_heightfeature.spinOnce();
  std::vector<particle> particles = m_mcl_heightfeature.getParticles();
  Eigen::VectorXf bestPose_xyz = cvt::eigen2xyzrpy(best_particle.pose);
//  std::cout << "[MCL]: callback node / pose:" << bestPose_xyz(0) << "," << bestPose_xyz(1) << "," << bestPose_xyz(5) * 180 / M_PI << "/N: " << particles.size() << std::endl;

  //m_pub_mclInfo
  m_bestpose = best_particle.pose;
  sa_mcl mclInfo;
  mclInfo.lidar = cvt::cloud2msg(m_cloud_feature);
  mclInfo.mclPose.pose.pose = cvt::eigen2geoPose(m_bestpose);
  m_pub_mclInfo.publish(mclInfo);

  // publish
  pubparticle(best_particle);
  publidar(m_cloud_feature,best_particle.pose);
  pubparticles(particles);
}

void mcl::callback_initPose(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg)
{
  Eigen::Matrix4f pose = cvt::geoPose2eigen(msg->pose.pose);
  Eigen::VectorXf pose_xyz = cvt::eigen2xyzrpy(pose);
  m_mcl_heightfeature.initParticle(cvt::geoPose2eigen(msg->pose.pose),0.2);
  std::cout << "[MCL]: callback initpose " << pose_xyz(0) << "," << pose_xyz(1) << "," << pose_xyz(5) * 180 / M_PI << std::endl;
}

void mcl::pubparticle(particle particleo)
{
    nav_msgs::Odometry msg_particle;
    msg_particle.header.frame_id="map";
    msg_particle.pose.pose = cvt::eigen2geoPose(particleo.pose);
    m_pub_mclPose.publish(msg_particle);
}

void mcl::publidar(pcl::PointCloud<pcl::PointXYZ> lidar, Eigen::Matrix4f pose)
{
  pcl::PointCloud<pcl::PointXYZ> lidar_global;
  pcl::transformPointCloud(lidar,lidar_global,pose);
  sensor_msgs::PointCloud2 msg_lidar = cvt::cloud2msg(lidar_global);
  msg_lidar.header.frame_id="map";
  m_pub_mclLiDAR.publish(msg_lidar);
}

void mcl::pubparticles(std::vector<particle> particles)
{
  pcl::PointCloud<pcl::PointXYZ> particlePts;
  for(int i=0;i<particles.size();i++)
  {
    pcl::PointXYZ tmp_xyz;
    tmp_xyz.x = particles.at(i).pose(0,3);
    tmp_xyz.y = particles.at(i).pose(1,3);
    tmp_xyz.z = 0;
    particlePts.push_back(tmp_xyz);
  }

  m_pub_mclParticles.publish(cvt::cloud2msg(particlePts));
}
