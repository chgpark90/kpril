#include "functional_module.h"

using namespace saslam;
using namespace unavlib;

void functional_module::DrawRotatedRectangle(cv::Mat& image, cv::Point centerPoint, cv::Size rectangleSize, double rotationDegrees, cv::Scalar value)
{
    // Create the rotated rectangle
    cv::RotatedRect rotatedRectangle(centerPoint, rectangleSize, rotationDegrees);

    // We take the edges that OpenCV calculated for us
    cv::Point2f vertices2f[4];
    rotatedRectangle.points(vertices2f);

    // Convert them so we can use them in a fillConvexPoly
    geometry_msgs::Polygon polygon_tmp;
    cv::Point vertices[4];
    for(int i = 0; i < 4; ++i){
        geometry_msgs::Point32 point;
        Eigen::Matrix<float, 4, 1> point_;

        vertices[i] = vertices2f[i];

        point_(0, 0) = vertices2f[i].x;
        point_(1, 0) = vertices2f[i].y;
        point_(2, 0) = 0;
        point_(3, 0) = 1;

        point_ = m_T_global2occuMap.inverse() * point_;
        point.x = point_(0, 0);
        point.y = point_(1, 0);
        point.z = 0;
        polygon_tmp.points.push_back(point);
    }
    m_polygon.polygon = polygon_tmp;

    // Now we can fill the rotated rectangle with our specified color
    cv::fillConvexPoly(image,
                       vertices,
                       4,
                       value);
}

functional_module::functional_module()
{
  getparam();

  m_sub_map = m_nh.subscribe("/saslam/functional_module/tag_detections", 10, &functional_module::callback_save_updated_map,this);
  m_sub_mcl = m_nh.subscribe("/saslam/functional_module/mcl", 10, &functional_module::callback_crt_state, this);
  m_sub_updated = m_nh.subscribe("/saslam/functional_module/updated_map_flag", 10, &functional_module::callback_map_updated, this);

  m_pub_updated_map = m_nh.advertise<std_msgs::Bool>("/saslam/functional_module/updated_map", 10);
  m_pub_polygon_0 = m_nh.advertise<geometry_msgs::PolygonStamped>("/saslam/functional_module/polygon0", 10);
  m_pub_polygon_1 = m_nh.advertise<geometry_msgs::PolygonStamped>("/saslam/functional_module/polygon1", 10);
  m_pub_polygon_2 = m_nh.advertise<geometry_msgs::PolygonStamped>("/saslam/functional_module/polygon2", 10);
  m_pub_polygon_3 = m_nh.advertise<geometry_msgs::PolygonStamped>("/saslam/functional_module/polygon3", 10);

  m_timer = m_nh.createTimer(ros::Duration(0.1), &functional_module::spinTimer,this);
  readMap(m_param_mapdir);

  m_updated_recv_gate = true;

  for (int idx = 0; idx < m_number_markers; idx++) m_detect_check_total[idx] = 0;
}

functional_module::~functional_module()
{
}

void functional_module::getparam()
{
    m_nh.param<std::string>("movingControl/updatedmapPath",m_param_updated_map,"~/.ros");
    m_nh.param<std::string>("movingControl/objectmapPath",m_param_mapdir,"~/.ros");
}

void functional_module::spinTimer(const ros::TimerEvent&) {
  if (m_updated_send_gate && m_updated_map_flag && m_updated_recv_gate) {
    std_msgs::Bool tmp;
    tmp.data = true;
    m_pub_updated_map.publish(tmp);
    m_updated_map_flag = false;
    m_updated_recv_gate = false;
  }
}

void functional_module::callback_map_updated(std_msgs::Bool::ConstPtr msg) {
  m_updated_recv_gate = true;
}

void functional_module::callback_crt_state(saslam::sa_mcl::ConstPtr msg) {
  Eigen::MatrixXf curPose = cvt::geoPose2eigen(msg->mclPose.pose.pose);
  m_curPose = curPose;
}

void functional_module::callback_save_updated_map(apriltag_ros::AprilTagDetectionArray::ConstPtr msg)
{
  static bool updated_inform_flag = false;
  m_updated_send_gate = false;

  // Marker detection filter
  bool find_flag;
  for (int idx = 0; idx < m_number_markers; idx++) {
    find_flag = false;
    for (int i = 0; i < msg->detections.size(); i++) {
      if (idx == msg->detections.at(i).id.front()) find_flag = true;
    }
    if (find_flag) {
      m_detect_check[idx].push(true);
      m_detect_check_total[idx]++;
    } else {
      m_detect_check[idx].push(false);
    }
  }

  // Marker queue deletion
  if (m_detect_check[0].size() > m_marker_queue) {
    for (int idx = 0; idx < m_number_markers; idx++) {
      if (m_detect_check[idx].front()) {
        m_detect_check_total[idx]--;
      }
      m_detect_check[idx].pop();
    }
  }

  for (int i = 0; i < msg->detections.size(); i++) {
    int idx = msg->detections.at(i).id.front();

    if (m_detect_check_total[idx] == m_marker_queue) { // Marker detection check in continuous 10 frame
      geometry_msgs::Pose tmp_pose;
      tmp_pose.position.x = msg->detections.at(i).pose.pose.pose.position.x;
      tmp_pose.position.y = msg->detections.at(i).pose.pose.pose.position.y;
      tmp_pose.position.z = msg->detections.at(i).pose.pose.pose.position.z;
      tmp_pose.orientation.w = msg->detections.at(i).pose.pose.pose.orientation.w;
      tmp_pose.orientation.x = msg->detections.at(i).pose.pose.pose.orientation.x;
      tmp_pose.orientation.y = msg->detections.at(i).pose.pose.pose.orientation.y;
      tmp_pose.orientation.z = msg->detections.at(i).pose.pose.pose.orientation.z;
      Eigen::MatrixXf tagPose = cvt::geoPose2eigen(tmp_pose);

      Eigen::MatrixXf curPoseOnOccuMap = m_T_global2occuMap * m_curPose * m_T_base2camera * tagPose;

      Eigen::Matrix<float, 4, 1> x_coord;
      x_coord << 1, 0, 0, 0;
      x_coord = curPoseOnOccuMap * x_coord;

      float x = curPoseOnOccuMap(0, 3);
      float y = curPoseOnOccuMap(1, 3);
      double theta = atan2(x_coord(1, 0), x_coord(0, 0)) / M_PI * 180;

      if (sqrt(pow(m_inform_prev[idx].x - x, 2) + pow(m_inform_prev[idx].y - y, 2)) > 4.
      || abs(m_inform_prev[idx].theta - theta) > 5.) {
        m_inform_prev[idx].x = x;
        m_inform_prev[idx].y = y;
        m_inform_prev[idx].theta = theta;
        continue;
      }
      m_inform_prev[idx].x = x;
      m_inform_prev[idx].y = y;
      m_inform_prev[idx].theta = theta;

      if (sqrt(pow(m_inform_valid[idx].x - x, 2) + pow(m_inform_valid[idx].y - y, 2)) < 2
      && abs(m_inform_valid[idx].theta - theta) < 4) {
        continue;
      }

      updated_inform_flag = true;
      // store information
      m_inform_valid[idx].valid = true;
      m_inform_valid[idx].x = x;
      m_inform_valid[idx].y = y;
      m_inform_valid[idx].theta = theta;
    }
  }

  if (updated_inform_flag && m_updated_recv_gate) {
    // Apply the information to map
    applyInform();
    saveUpdatedMap(m_param_updated_map);
    updated_inform_flag = false;
    m_updated_map_flag = true;
  }
  m_updated_send_gate = true;

}

void functional_module::applyInform()
{
  m_functional_map = cv::Mat::zeros(m_occumap.info.height, m_occumap.info.width, CV_8UC1);

  for (int idx = 0; idx < m_number_markers; idx++) {
    cv::Mat tmp_map = cv::Mat::zeros(m_occumap.info.height, m_occumap.info.width, CV_8UC1);
    if (m_inform_valid[idx].valid) {
      m_polygon.header.frame_id = "/map";
      DrawRotatedRectangle(tmp_map, cv::Point((int)m_inform_valid[idx].x, (int)m_inform_valid[idx].y),
                           cv::Size(1 / m_occumap.info.resolution, 1 / m_occumap.info.resolution), m_inform_valid[idx].theta, cv::Scalar(m_value_casting.val));

      m_value_casting.val = 0;
      if (idx == 0) {
          m_pub_polygon_0.publish(m_polygon);
          m_value_casting.id_0 = 1;
      } else if (idx == 1) {
          m_pub_polygon_1.publish(m_polygon);
          m_value_casting.id_1 = 1;
      } else if (idx == 2) {
          m_pub_polygon_2.publish(m_polygon);
          m_value_casting.id_2 = 1;
      } else if (idx == 3) {
          m_pub_polygon_3.publish(m_polygon);
          m_value_casting.id_3 = 1;
      }
    }
    m_functional_map = m_functional_map + tmp_map;
  }
  m_updated_occumap = m_occumap;

  for (int pt = 0; pt < m_updated_occumap.data.size(); pt++)
  {
    int pt_y = pt / m_updated_occumap.info.width;
    int pt_x = pt % m_updated_occumap.info.width;
    m_value_casting.val = m_functional_map.at<unsigned char>(pt_y, pt_x);
    if (m_value_casting.id_2 == 1) m_updated_occumap.data.at(pt) = 100;
  }
  for (int idx = 0; idx < m_number_markers; idx++) {
    if (m_inform_valid[idx].valid) {
      int pt = (int)m_inform_valid[idx].y * m_updated_occumap.info.width + (int)m_inform_valid[idx].x;
      try {
        m_updated_occumap.data.at(pt) = 100;
      } catch (const std::exception &e) {
        ;
      }
    }
  }
}

void functional_module::saveUpdatedMap(std::string path)
{
  std::stringstream sstmG;
  sstmG<<path<<"/occumap";
  cvt::saveOccupanymap(sstmG.str(), m_updated_occumap);
}

void functional_module::readMap(std::string path)
{
  std::stringstream sstmG;
  sstmG<<path<<"/occumap";
  std::cout<<"[FUNC] LOAD OCCU MAP : "<<sstmG.str()<<std::endl;
  cvt::loadOccupancymap(sstmG.str(),m_occumap);
  m_occumap.header.frame_id="map";


  tf::StampedTransform transform;
  bool get_tf = true;
  while (get_tf) {
    try {
      m_listener.lookupTransform("base_link", "head_camera", ros::Time(0), transform);
      m_T_base2camera = cvt::tf2eigen(transform);
      get_tf = false;
    } catch (tf::TransformException e) {
      ;
    }
  }

  float resolution = m_occumap.info.resolution;
  resolution = resolution;
  m_T_global2occuMap = Eigen::Matrix4f::Identity();
  m_T_global2occuMap(0,0) = 1./resolution;
  m_T_global2occuMap(1,1) = 1./resolution;
  m_T_global2occuMap(0,3) = -m_occumap.info.origin.position.x / resolution;
  m_T_global2occuMap(1,3) = -m_occumap.info.origin.position.y / resolution;
}

