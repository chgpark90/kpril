#ifndef FUNCTIONAL_MODULE_H
#define FUNCTIONAL_MODULE_H
#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PolygonStamped.h>

#include <queue>
#include "apriltag_ros/AprilTagDetectionArray.h"
#include "saslam/sa_mcl.h"
#include "unavlib/convt.h"
#include "unavlib/others.h"

#define NUM_MARKER 4

union U {
    struct {
        int id_0 : 2;
        int id_1 : 2;
        int id_2 : 2;
        int id_3 : 2;
    };
    unsigned char val;
};

namespace saslam
{
    class functional_module
    {

    struct s_information{
      bool valid = false;
      float x = 0;
      float y = 0;
      double theta = 0.0;
    };

    private:
      ros::NodeHandle m_nh;
      ros::Subscriber m_sub_map;
      ros::Subscriber m_sub_mcl;
      ros::Subscriber m_sub_updated;
      ros::Publisher m_pub_updated_map;
      ros::Publisher m_pub_polygon_0;
      ros::Publisher m_pub_polygon_1;
      ros::Publisher m_pub_polygon_2;
      ros::Publisher m_pub_polygon_3;

      ros::Timer m_timer;

      tf::TransformListener m_listener;
      Eigen::Matrix4f m_T_base2camera;

      U m_value_casting;

      std::string m_param_mapdir;
      std::string m_param_updated_map;
      nav_msgs::OccupancyGrid m_occumap;
      nav_msgs::OccupancyGrid m_updated_occumap;
      cv::Mat m_functional_map;
      geometry_msgs::PolygonStamped m_polygon;

      Eigen::MatrixXf m_curPose;
      Eigen::MatrixXf m_T_global2occuMap;

      unsigned int m_number_markers = NUM_MARKER;
      unsigned int m_marker_queue = 10;
      std::queue<bool> m_detect_check[NUM_MARKER];
      s_information m_inform_valid[NUM_MARKER];
      s_information m_inform_prev[NUM_MARKER];
      int m_detect_check_total[NUM_MARKER];

      bool m_updated_map_flag;
      bool m_updated_send_gate;
      bool m_updated_recv_gate;

      void getparam(); /**< To get parameter from launch file */

      void spinTimer(const ros::TimerEvent &);
      void callback_map_updated(std_msgs::Bool::ConstPtr msg);
      void callback_save_updated_map(apriltag_ros::AprilTagDetectionArray::ConstPtr msg); /**< Node subscribe */
      void callback_crt_state(saslam::sa_mcl::ConstPtr msg); /**< Node subscribe */
      void applyInform();
      void saveUpdatedMap(std::string path);
      void readMap(std::string path);

      void DrawRotatedRectangle(cv::Mat& image, cv::Point centerPoint, cv::Size rectangleSize, double rotationDegrees, cv::Scalar value);
    public:
      functional_module();
      ~functional_module();
    };

}


#endif

