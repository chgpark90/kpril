#include <ros/ros.h>
#include "src/functional_module.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mm_functional_module");
    saslam::functional_module func_c = saslam::functional_module();
    ros::spin();

    return 0;
}
